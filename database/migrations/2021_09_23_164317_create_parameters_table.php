<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters', function (Blueprint $table) {
            $table->id();

//            $table->unsignedBigInteger('product_id');
//            $table->foreign('product_id')->references('id')->on('products');

            $table->integer('size')->nullable();
            $table->float('weight',60)->nullable();
            $table->float('price',60)->nullable();
            $table->float('calories',60);

//            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameters');
    }
}
