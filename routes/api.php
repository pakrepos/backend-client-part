<?php

use App\Http\Controllers\AddressController;
use App\Http\Controllers\ParameterController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//
Route::post('/registration', [UserController::class, 'register']);
Route::post('/getToken', [UserController::class, 'getToken']);

Route::middleware(['api', 'auth:sanctum'])->group(function () {
    Route::group(['prefix' => 'user'], function () {
        Route::get('/me', [UserController::class, 'me']);
        Route::delete('/dropToken', [UserController::class, 'dropToken']);
        Route::patch('/update', [UserController::class, 'update']);


        Route::delete('/delete/{id}', [UserController::class, 'delete']);
    });

    Route::group(['prefix' => 'address'], function () {
        Route::post('/create', [AddressController::class, 'create']);
        Route::get('/all', [AddressController::class, 'all']);
        Route::patch('/update/{id}', [AddressController::class, 'update']);
        Route::get('/default', [AddressController::class, 'default']);
        Route::delete('/delete/{id}', [AddressController::class, 'destroy']);
    });

    Route::group(['prefix' => 'parameter'], function () {
        Route::post('/create', [ParameterController::class, 'create']);
    });
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

