<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Illuminate\Http\Request;
use function PHPUnit\Framework\isEmpty;

class AddressController extends Controller
{
    public function create(Request $request)
    {
        $validate = $request->validate([
            'address' => 'required',
        ]);

        $check = Address::query()->firstOrCreate(
            ['address' => $validate['address'], 'user_id' => auth()->id()]
        )->wasRecentlyCreated;

        if ($check) {
            return response()->json([]);
        }
        return response()->json(['error' => "Адресс уже существует."], 409);
    }

    public function all()
    {
        $addresses = Address::query()->where('user_id', auth()->id())->get();
        return response()->json(['address' => $addresses]);
    }

    public function default()
    {
        $address = Address::query()->where('user_id', auth()->id())
            ->where('default', true)->get();

        if ($address->isNotEmpty()) {
            return response()->json(['address' => $address]);
        }
        return response()->json([], 404);
    }

    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'address' => 'sometimes',
            'default' => 'sometimes',
        ]);

        $address = Address::query()->find($id);

        if ($address) {
            $address->update($validate);
            return response('');
        }
        return response()->json(['error' => "адресс не найдет"], 404);

    }

    public function destroy($id)
    {
        $address = Address::query()->find($id);

        if (!empty($address)) {
            $address->delete();
            return response('');
        }
        return response()->json(['error' => "адресс не найдет"], 404);
    }

}
