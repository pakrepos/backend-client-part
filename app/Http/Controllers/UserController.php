<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * регистрация
     */
    public function register(Request $request)
    {

        $validate = $request->validate([
            'second_name' => 'required',
            'first_name' => 'required',
            'last_name' => 'sometimes',
            'patronymic' => 'sometimes',
            'email' => 'required',
            'password' => 'required',
        ]);

        $CheckUserEmail = User::all('email')->where('email', $request['email']);
        if ($CheckUserEmail->isEmpty()) {

            $user = User::create($validate);
            $token = $user->createToken($request['email'])->plainTextToken;

            return response()->json(["success" => true,
                "user" => $user,
                "token" => $token]);

        }
        return response()->json(["success" => false,
            "error" => "Данный Email уже зарегестрирован"], 409);
    }

    /**
     * авторизация пользователя
     */
    public function getToken(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = User::all()
            ->where('email', $request['email'])
            ->where('password', $request['password'])
            ->first();


        if (!$user) {
            return response()->json(["success" => false,
                "error" => "Неверный логин или пароль"], 404);
        }

        $token = $user->createToken($request['email'])->plainTextToken;

        return response()->json(["token" => $token]);
    }

    /**
     * возращает авторизированного пользователя
     */
    public function me()
    {
        $user = auth()->user()->toArray();
        unset($user['id']);

        return response()->json(["success" => true,
            "user" => $user]);
    }

    /**
     * разлогирование пользователя
     */
    public function dropToken()
    {
        auth()->user()->currentAccessToken()->delete();

        return response()->json(["success" => true]);
    }

    /**
     * обновить данные авторизированного пользователя
     */
    public function update(Request $request)
    {
        $validate = $request->validate([
            'address_id' => 'sometimes',
            'phone' => 'sometimes',
            'name' => 'sometimes',
            'first_name' => 'sometimes',
            'last_name' => 'sometimes',
        ]);

        auth()->user()->update($validate);
        return response()->json(["success" => true]);
    }


    /**
     * удалить авторизированного пользователя
     */
    public function delete($id)
    {
        $user = User::query()->find($id);
        $user->delete();
        return response()->json(["success" => true]);
    }
}
