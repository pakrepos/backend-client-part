<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ParameterController extends Controller
{
    public function create(Request $request)
    {
        $validate = $request->validate([
            'address' => 'required',
        ]);

        $check = Address::query()->firstOrCreate(
            ['address' => $validate['address'], 'user_id' => auth()->id()]
        )->wasRecentlyCreated;

        if ($check) {
            return response()->json([]);
        }
        return response()->json(['error' => "Адресс уже существует."], 409);
    }
}
