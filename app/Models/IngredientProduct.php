<?php

namespace App\Models;

use App\Models\Base\IngredientProduct as BaseIngredientProduct;

class IngredientProduct extends BaseIngredientProduct
{
	protected $fillable = [
		'ingredient_id',
		'product_id'
	];
}
