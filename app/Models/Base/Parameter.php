<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Parameter
 * 
 * @property int $id
 * @property int|null $size
 * @property float|null $weight
 * @property float|null $price
 * @property float $calories
 * 
 * @property Collection|Product[] $products
 *
 * @package App\Models\Base
 */
class Parameter extends Model
{
	protected $table = 'parameters';
	public $timestamps = false;

	protected $casts = [
		'size' => 'int',
		'weight' => 'float',
		'price' => 'float',
		'calories' => 'float'
	];

	public function products()
	{
		return $this->hasMany(Product::class);
	}
}
