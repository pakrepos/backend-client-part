<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * 
 * @property int $id
 * @property string $name
 * @property string $color
 * 
 * @property Collection|Product[] $products
 *
 * @package App\Models\Base
 */
class Category extends Model
{
	protected $table = 'categories';
	public $timestamps = false;

	public function products()
	{
		return $this->belongsToMany(Product::class)
					->withPivot('id');
	}
}
