<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PromoCode
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $code
 * @property array $parameters
 *
 * @package App\Models\Base
 */
class PromoCode extends Model
{
	protected $table = 'promo_codes';
	public $timestamps = false;

	protected $casts = [
		'parameters' => 'json'
	];
}
