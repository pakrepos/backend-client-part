<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CartProduct
 * 
 * @property int $id
 * @property int $cart_id
 * @property int $product_id
 * 
 * @property Cart $cart
 * @property Product $product
 *
 * @package App\Models\Base
 */
class CartProduct extends Model
{
	protected $table = 'cart_product';
	public $timestamps = false;

	protected $casts = [
		'cart_id' => 'int',
		'product_id' => 'int'
	];

	public function cart()
	{
		return $this->belongsTo(Cart::class);
	}

	public function product()
	{
		return $this->belongsTo(Product::class);
	}
}
