<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductUser
 * 
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @property array|null $specifications
 * 
 * @property Product $product
 * @property User $user
 *
 * @package App\Models\Base
 */
class ProductUser extends Model
{
	protected $table = 'product_user';
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int',
		'product_id' => 'int',
		'specifications' => 'json'
	];

	public function product()
	{
		return $this->belongsTo(Product::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
