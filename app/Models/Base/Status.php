<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Status
 * 
 * @property int $id
 * @property string $name
 * 
 * @property Collection|Order[] $orders
 *
 * @package App\Models\Base
 */
class Status extends Model
{
	protected $table = 'statuses';
	public $timestamps = false;

	public function orders()
	{
		return $this->hasMany(Order::class);
	}
}
