<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Product;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductTag
 * 
 * @property int $id
 * @property int $tag_id
 * @property int $product_id
 * 
 * @property Product $product
 * @property Tag $tag
 *
 * @package App\Models\Base
 */
class ProductTag extends Model
{
	protected $table = 'product_tag';
	public $timestamps = false;

	protected $casts = [
		'tag_id' => 'int',
		'product_id' => 'int'
	];

	public function product()
	{
		return $this->belongsTo(Product::class);
	}

	public function tag()
	{
		return $this->belongsTo(Tag::class);
	}
}
