<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Ingredient;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

/**
 * Class IngredientProduct
 * 
 * @property int $id
 * @property int $ingredient_id
 * @property int $product_id
 * 
 * @property Ingredient $ingredient
 * @property Product $product
 *
 * @package App\Models\Base
 */
class IngredientProduct extends Model
{
	protected $table = 'ingredient_product';
	public $timestamps = false;

	protected $casts = [
		'ingredient_id' => 'int',
		'product_id' => 'int'
	];

	public function ingredient()
	{
		return $this->belongsTo(Ingredient::class);
	}

	public function product()
	{
		return $this->belongsTo(Product::class);
	}
}
