<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ingredient
 * 
 * @property int $id
 * @property string $name
 * @property float $price
 * @property float $calories
 * @property int|null $count
 * @property float|null $weight
 * 
 * @property Collection|Product[] $products
 *
 * @package App\Models\Base
 */
class Ingredient extends Model
{
	protected $table = 'ingredients';
	public $timestamps = false;

	protected $casts = [
		'price' => 'float',
		'calories' => 'float',
		'count' => 'int',
		'weight' => 'float'
	];

	public function products()
	{
		return $this->belongsToMany(Product::class)
					->withPivot('id');
	}
}
