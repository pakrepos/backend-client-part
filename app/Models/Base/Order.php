<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Product;
use App\Models\Status;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * 
 * @property int $id
 * @property int $order_id
 * @property int $user_id
 * @property int|null $delivery_id
 * @property float $order_price
 * @property string $address
 * @property string $phone
 * @property int $status_id
 * @property int $product_id
 * @property int|null $count
 * @property array|null $specifications
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property User $user
 * @property Product $product
 * @property Status $status
 *
 * @package App\Models\Base
 */
class Order extends Model
{
	protected $table = 'orders';

	protected $casts = [
		'order_id' => 'int',
		'user_id' => 'int',
		'delivery_id' => 'int',
		'order_price' => 'float',
		'status_id' => 'int',
		'product_id' => 'int',
		'count' => 'int',
		'specifications' => 'json'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function product()
	{
		return $this->belongsTo(Product::class);
	}

	public function status()
	{
		return $this->belongsTo(Status::class);
	}
}
