<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Offer
 * 
 * @property int $id
 * @property int $product_id
 * @property boolean|null $image
 * @property float $discount
 * @property int $day_week
 * 
 * @property Product $product
 *
 * @package App\Models\Base
 */
class Offer extends Model
{
	protected $table = 'offers';
	public $timestamps = false;

	protected $casts = [
		'product_id' => 'int',
		'image' => 'boolean',
		'discount' => 'float',
		'day_week' => 'int'
	];

	public function product()
	{
		return $this->belongsTo(Product::class);
	}
}
