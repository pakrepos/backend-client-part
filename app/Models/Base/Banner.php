<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Banner
 * 
 * @property int $id
 * @property boolean $image
 * @property string $description
 * @property bool $is_active
 *
 * @package App\Models\Base
 */
class Banner extends Model
{
	protected $table = 'banners';
	public $timestamps = false;

	protected $casts = [
		'image' => 'boolean',
		'is_active' => 'bool'
	];
}
