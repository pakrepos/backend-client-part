<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Ingredient;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Parameter;
use App\Models\Tag;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * 
 * @property int $id
 * @property int $parameter_id
 * @property boolean $image
 * @property string $name
 * @property Carbon|null $cooking_time
 * @property float $discount
 * @property bool $is_active
 * 
 * @property Parameter $parameter
 * @property Collection|Cart[] $carts
 * @property Collection|Category[] $categories
 * @property Collection|Ingredient[] $ingredients
 * @property Collection|Offer[] $offers
 * @property Collection|Order[] $orders
 * @property Collection|Tag[] $tags
 * @property Collection|User[] $users
 *
 * @package App\Models\Base
 */
class Product extends Model
{
	protected $table = 'products';
	public $timestamps = false;

	protected $casts = [
		'parameter_id' => 'int',
		'image' => 'boolean',
		'discount' => 'float',
		'is_active' => 'bool'
	];

	protected $dates = [
		'cooking_time'
	];

	public function parameter()
	{
		return $this->belongsTo(Parameter::class);
	}

	public function carts()
	{
		return $this->belongsToMany(Cart::class)
					->withPivot('id');
	}

	public function categories()
	{
		return $this->belongsToMany(Category::class)
					->withPivot('id');
	}

	public function ingredients()
	{
		return $this->belongsToMany(Ingredient::class)
					->withPivot('id');
	}

	public function offers()
	{
		return $this->hasMany(Offer::class);
	}

	public function orders()
	{
		return $this->hasMany(Order::class);
	}

	public function tags()
	{
		return $this->belongsToMany(Tag::class)
					->withPivot('id');
	}

	public function users()
	{
		return $this->belongsToMany(User::class)
					->withPivot('id', 'specifications');
	}
}
