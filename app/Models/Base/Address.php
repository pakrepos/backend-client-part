<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Address
 *
 * @property int $id
 * @property string $address
 * @property bool $default
 *
 * @property Collection|User[] $users
 *
 * @package App\Models\Base
 */
class Address extends Model
{
	protected $table = 'addresses';
	public $timestamps = false;

	protected $casts = [
		'default' => 'bool',
        'user_id' => 'int'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
