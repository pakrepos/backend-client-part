<?php

namespace App\Models;

use App\Models\Base\Product as BaseProduct;

class Product extends BaseProduct
{
	protected $fillable = [
		'parameter_id',
		'image',
		'name',
		'cooking_time',
		'discount',
		'is_active'
	];
}
