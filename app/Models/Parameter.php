<?php

namespace App\Models;

use App\Models\Base\Parameter as BaseParameter;

class Parameter extends BaseParameter
{
	protected $fillable = [
		'size',
		'weight',
		'price',
		'calories'
	];
}
