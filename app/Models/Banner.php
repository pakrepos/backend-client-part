<?php

namespace App\Models;

use App\Models\Base\Banner as BaseBanner;

class Banner extends BaseBanner
{
	protected $fillable = [
		'image',
		'description',
		'is_active'
	];
}
