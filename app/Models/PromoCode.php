<?php

namespace App\Models;

use App\Models\Base\PromoCode as BasePromoCode;

class PromoCode extends BasePromoCode
{
	protected $fillable = [
		'name',
		'description',
		'code',
		'parameters'
	];
}
