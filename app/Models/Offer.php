<?php

namespace App\Models;

use App\Models\Base\Offer as BaseOffer;

class Offer extends BaseOffer
{
	protected $fillable = [
		'product_id',
		'image',
		'discount',
		'day_week'
	];
}
