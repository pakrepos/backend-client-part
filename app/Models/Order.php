<?php

namespace App\Models;

use App\Models\Base\Order as BaseOrder;

class Order extends BaseOrder
{
	protected $fillable = [
		'order_id',
		'user_id',
		'delivery_id',
		'order_price',
		'address',
		'phone',
		'status_id',
		'product_id',
		'count',
		'specifications'
	];
}
