<?php

namespace App\Models;

use App\Models\Base\User as BaseUser;

class User extends BaseUser
{
	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'phone',
		'name',
		'first_name',
		'last_name',
		'date_of_birth',
		'email',
		'email_verified_at',
		'password',
		'remember_token'
	];
}
