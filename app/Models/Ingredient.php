<?php

namespace App\Models;

use App\Models\Base\Ingredient as BaseIngredient;

class Ingredient extends BaseIngredient
{
	protected $fillable = [
		'name',
		'price',
		'calories',
		'count',
		'weight'
	];
}
