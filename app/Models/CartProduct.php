<?php

namespace App\Models;

use App\Models\Base\CartProduct as BaseCartProduct;

class CartProduct extends BaseCartProduct
{
	protected $fillable = [
		'cart_id',
		'product_id'
	];
}
