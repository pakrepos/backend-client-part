<?php

namespace App\Models;

use App\Models\Base\CategoryProduct as BaseCategoryProduct;

class CategoryProduct extends BaseCategoryProduct
{
	protected $fillable = [
		'category_id',
		'product_id'
	];
}
