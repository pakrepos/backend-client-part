<?php

namespace App\Models;

use App\Models\Base\ProductUser as BaseProductUser;

class ProductUser extends BaseProductUser
{
	protected $fillable = [
		'user_id',
		'product_id',
		'specifications'
	];
}
