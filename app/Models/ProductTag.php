<?php

namespace App\Models;

use App\Models\Base\ProductTag as BaseProductTag;

class ProductTag extends BaseProductTag
{
	protected $fillable = [
		'tag_id',
		'product_id'
	];
}
