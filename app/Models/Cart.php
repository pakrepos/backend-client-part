<?php

namespace App\Models;

use App\Models\Base\Cart as BaseCart;

class Cart extends BaseCart
{
	protected $fillable = [
		'user_id',
		'product_id',
		'specifications'
	];
}
